The purpose of my Lambda funtion is to find the opposite value of the input number.

Successful deployment on AWS:

![sample1](Deployment.png)

Test on positive number:

![sample1](Positive.png)

Test on negative number:

![sample1](Negative.png)