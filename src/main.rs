use lambda_runtime::{run, service_fn, Error, LambdaEvent};

 use serde::{Deserialize, Serialize};
 
 #[derive(Deserialize)]
 struct Request {
     number: i32,
 }
 
 #[derive(Serialize)]
 struct Response {
     opposite: i32,
 }
 
 async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
     let opposite = event.payload.number * -1;
 
     let resp = Response { opposite };
 
     Ok(resp)
 }
 
 #[tokio::main]
 async fn main() -> Result<(), Error> {
     tracing_subscriber::fmt()
         .with_max_level(tracing::Level::INFO)
         .with_target(false)
         .without_time()
         .init();
 
     run(service_fn(function_handler)).await
 }
 