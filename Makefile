rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

watch:
	cargo lambda watch

invoke:
	cargo lambda invoke --data-ascii "{ \"number\": 25}"

build:
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-1

aws-invoke:
	cargo lambda invoke --remote new-lambda-project --data-ascii "{ \"total\": 10}"

run:
	cargo run

release:
	cargo build --release

all: format lint test run
